package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class RunRemoteTaskCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(RunRemoteTaskCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    RunRemoteTask command = new RunRemoteTask();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(!(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("bot") && parameters.get("bot") != null && parameters.get("bot").get() != null) {
      convertedParameters.put("bot", parameters.get("bot").get());
      if(!(convertedParameters.get("bot") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","bot", "String", parameters.get("bot").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("bot") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","bot"));
    }

    if(parameters.containsKey("runner") && parameters.get("runner") != null && parameters.get("runner").get() != null) {
      convertedParameters.put("runner", parameters.get("runner").get());
      if(!(convertedParameters.get("runner") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","runner", "String", parameters.get("runner").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("runner") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","runner"));
    }

    if(parameters.containsKey("pool") && parameters.get("pool") != null && parameters.get("pool").get() != null) {
      convertedParameters.put("pool", parameters.get("pool").get());
      if(!(convertedParameters.get("pool") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","pool", "String", parameters.get("pool").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("pool") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","pool"));
    }

    if(parameters.containsKey("inputdict") && parameters.get("inputdict") != null && parameters.get("inputdict").get() != null) {
      convertedParameters.put("inputdict", parameters.get("inputdict").get());
      if(!(convertedParameters.get("inputdict") instanceof Map)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","inputdict", "Map", parameters.get("inputdict").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("port") && parameters.get("port") != null && parameters.get("port").get() != null) {
      convertedParameters.put("port", parameters.get("port").get());
      if(!(convertedParameters.get("port") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","port", "Number", parameters.get("port").get().getClass().getSimpleName()));
      }
    }

    command.setSessions(sessionMap);
    command.setGlobalSessionContext(globalSessionContext);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("bot"),(String)convertedParameters.get("runner"),(String)convertedParameters.get("pool"),(Map<String, Value>)convertedParameters.get("inputdict"),(Number)convertedParameters.get("port")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
