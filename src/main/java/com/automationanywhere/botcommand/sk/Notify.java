/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;

import BotUtils.BotMessage;
import javafx.application.Platform;
import javafx.collections.ObservableList;

import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.stage.Window;




/**
 * @author Stefan Karsten
 *
 */



@BotCommand
@CommandPkg(label = "Notify", name = "Notify", description = "Notify Launchpad",  comment = true ,  text_color = "#666691" , background_color =  "#666691" , 
icon = "alp.svg", node_label = "Notify") 
public class Notify {
 

	private static Logger logger = LogManager.getLogger(Notify.class);

    
    
    @Execute
    public void notify(@Idx(index = "1", type = TEXT) @Pkg(label = "Message", 
    default_value_type = STRING) @NotEmpty String message,
    					@Idx(index = "2", type = SELECT, options = {
    					@Idx.Option(index = "2.1", pkg = @Pkg(label = "Information", value = "INFORMATION")),
						@Idx.Option(index = "2.2", pkg = @Pkg(label = "Error", value = "ERROR")),
						@Idx.Option(index = "2.3", pkg = @Pkg(label = "Warning", value = "WARNING")),
						@Idx.Option(index = "2.4", pkg = @Pkg(label = "Question", value = "QUESTION")),
						@Idx.Option(index = "2.5", pkg = @Pkg(label = "Start", value = "START")),
						@Idx.Option(index = "2.6", pkg = @Pkg(label = "Stop", value = "STOP")),
			         	}) @Pkg(label = "Message Type", default_value = "INFORMATION", default_value_type = STRING) @NotEmpty String type) throws Exception {
        
        Platform.runLater(new Runnable() {;
        @Override
        public void run() {
        	
        	
        	
        	ObservableList<Window> windows = Stage.getWindows();
        	for (Window win : windows) {
        		
                String topwindow = "LaunchpadListMessages";
                
                logger.info("ChoosenWindow: {}",topwindow);
                
                logger.info("Found Window: {}",win.getScene().getRoot().toString() );
        		
				if (win.getScene().lookup("#"+topwindow) != null)
				{
					TableView<BotMessage> list =  (TableView<BotMessage>) win.getScene().lookup("#"+topwindow);
					Button startbutton = (Button) win.getScene().lookup("#LaunchpadStartButton");
					if (type == "START" && startbutton != null) {
						startbutton.setDisable(true);
					}
					
					if ((type == "STOP" || type == "ERROR") && startbutton != null) {
						startbutton.setDisable(false);
					}
					

						
	                logger.info("List: {}",list.toString());

					if (list != null) {
						DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
						LocalDateTime now = LocalDateTime.now();  
       	             	BotMessage botmessage = new BotMessage(new String(type),new String(message),new String(now.format(formatter)));
       	                logger.info("Bot Message: {}", botmessage.getMessage());
						list.getItems().add(0,botmessage);

					}
		        }
        	}
          }

        });
    }
    
 
      
    
}