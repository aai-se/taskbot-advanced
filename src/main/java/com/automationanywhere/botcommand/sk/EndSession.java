/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.lang.Thread.State;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.bot.service.GlobalSessionContext.ChildBot;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;



/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "End session", name = "endSession", description = "End session", icon = "alp.svg", comment = true ,  text_color = "#666691" , background_color =  "#666691" ,  
node_label = "End session {{sessionName}}|")
public class EndSession {

	  @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	  private GlobalSessionContext globalSessionContext;

	  
	  public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	    }

    @Sessions
    private Map<String, Object> sessions;
    
	private static final Logger logger = LogManager.getLogger(EndSession.class);
 
    @Execute
    public void end(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = AttributeType.BOOLEAN) @Pkg(label = "Interrupt Threads", default_value_type = DataType.BOOLEAN , default_value = "false") @NotEmpty Boolean stopThreads,
    		@Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "Delay time (ms)", default_value_type = DataType.NUMBER , default_value = "1000" )  Number delay) throws Exception {
        	
        	
    	
    	Integer delaytime = (delay == null ) ? 1000:  delay.intValue();
        TaskBotSession taskbotsession = ((TaskBotSession)sessions.get(sessionName));
        Launchpad launchpad = taskbotsession.getLaunchPad();
        if (launchpad != null) {
        	launchpad.release();
        }
        List<Thread> threads = taskbotsession.getBotThreads();
        State state;
        if (stopThreads)
        {
        	Thread.sleep(delaytime);
            Boolean allStopped = false;
            while (!allStopped) {
             	allStopped =true;
                for (Iterator iterator = threads.iterator(); iterator.hasNext();) {
    				Thread thread = (Thread) iterator.next();
    				logger.info("Interrupt Thread Name "+thread.getName());
    				logger.info("Interrupt Thread State "+thread.getState().toString());
    				String botpath = taskbotsession.getBots().get(thread.getName()).toString();
    			    String taskbotUri = TaskBotSession.baseURI+botpath.replace(" ", "%20");
    			    ChildBot bot = this.globalSessionContext.getChildBotWithGlobalSessionContext(taskbotUri);
    			    bot.getGlobalSessionContext().getBotControl().pauseBot();				
    				state = thread.getState() ;
    				if (state  != Thread.State.TERMINATED ) {
    					allStopped =false;
    					thread.interrupt();
    				}
    			}
                
                Thread.sleep(delaytime);		
            }
			
        }
        else {
  
        	Thread.sleep(delaytime);

        	Boolean allStopped = false;
        	while (!allStopped) {
        		allStopped =true;
        		for (Iterator iterator = threads.iterator(); iterator.hasNext();) {
        			Thread thread = (Thread) iterator.next();
        			state = thread.getState() ;
        			logger.info("Check Status Thread Name "+thread.getName());
        			logger.info("Check Status Thread State "+thread.getState().toString());
        			if (state  != Thread.State.TERMINATED || state == Thread.State.RUNNABLE) {
        				allStopped =false;
        			}
        		}
            
        		Thread.sleep(delaytime);       		
        	}
        }
        
        sessions.remove(sessionName);
   }

    
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}