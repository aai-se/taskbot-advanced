/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;



import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.data.Value;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.Map.Entry;



/**
 * @author Stefan Karsten
 * 
 * Each child bot should be added as dependency to the parent bot
 *
 */



@BotCommand
@CommandPkg(label="Run Parallel Task", name="customrunparalleltask", description="Run Parallel Task by name", icon = "alp.svg",  comment = true , text_color = "#666691" , background_color =  "#666691" , 
node_label="Custom Run Parallel Task")
public class RunParallelTask {
	
	  private static final Logger logger = LogManager.getLogger(RunParallelTask.class);
	  
	  @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	  private GlobalSessionContext globalSessionContext;

	  
	  public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	    }

	  @Sessions
	  private Map<String, Object> sessions;
	  
	@Execute
	public void action( @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default" ) @NotEmpty String sessionName,
						@Idx (index = "2", type = TEXT) @Pkg(label = "Bot Name", default_value_type = STRING) @NotEmpty String botname,
						@Idx(index = "3", type = AttributeType.DICTIONARY )  @Pkg(label = "Bot Input"  , default_value_type = DataType.DICTIONARY )  Map<String,Value> inputdict)
	 {
		
		 TaskBotSession taskbotsession= ((TaskBotSession)this.sessions.get(sessionName));

		 Map<String, Value> bots = taskbotsession.getBots();
		 String botpath = taskbotsession.getBots().get(botname).toString();	
  		 Runnable runnable =  (Runnable) new ParallelTask(botpath,globalSessionContext,inputdict);
		 Thread thread = new Thread(runnable);
		 thread.setName(botname);
		 taskbotsession.addBotThread(thread);
		 thread.start();

	}
	
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	    
	  
	}


