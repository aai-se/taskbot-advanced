package com.automationanywhere.botcommand.sk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.automationanywhere.botcommand.data.Value;

public class TaskBotSession {
	
	public static String baseURI = "repository:///Automation%20Anywhere/Bots/";
    
    
	private Launchpad launchpad;
	private  Map<String, Value> bots;
	private List<Thread> botthreads;

	public TaskBotSession(Boolean createLaunchpad,Map<String, Value> bots) throws Exception {
        this.bots = bots;
        if (createLaunchpad) {
        	this.launchpad = new Launchpad(this.bots);
        	this.launchpad.aquire();
        }
        this.botthreads = new ArrayList<Thread>();
	}
	
	public List<Thread> getBotThreads() {
		return this.botthreads;
	}
	
	public Launchpad getLaunchPad() {
		return this.launchpad;
	}
	
	public Map<String, Value> getBots() {
		return this.bots;
	}
	
	public void  addBotThread(Thread botThread) {
		this.botthreads.add(botThread);
	}
}
