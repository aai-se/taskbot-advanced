package com.automationanywhere.botcommand.sk;


import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.bot.service.Bot;
import com.automationanywhere.bot.service.BotShutdownHook;
import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.bot.service.GlobalSessionContext.ChildBot;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;

public class ParallelTask implements Runnable {
	
	private String botpath;
	private Boolean continueOnError;
	private GlobalSessionContext context;
	private static final Logger logger = LogManager.getLogger(ParallelTask.class);
	private  Map<String,Value> input;
	
	
	public ParallelTask(String botpath, GlobalSessionContext context,  Map<String,Value> inputdict) { 
		this.botpath = botpath;
		this.context = context;
		this.input = inputdict;
	}
	

    public void run() {
       try {
		runTask();
       } catch (Exception e) {
    	   String message;
    	   if (e.getMessage() != null && !e.getMessage().isEmpty()) {
    		   message = e.getMessage();
    	   }
    	   else { 
    		   message = String.format("Task Bot %s threw an exception : ", new Object[] { e.getClass().getSimpleName() });
    	   } 
    	   logger.error(message, e);
    	   if (!this.continueOnError.booleanValue()) {
    		   throw new BotCommandException(message, e);
    	   }
       }
    }
    
    
    
    public Value runTask() throws Exception 
	{
	    
	    String taskbotUri = TaskBotSession.baseURI+botpath.replace(" ", "%20");
	    logger.info("Task Bot URI : "+taskbotUri);
	    
	   ChildBot bot = this.context.getChildBotWithGlobalSessionContext(taskbotUri);
	    Optional<Value> outputValue = Optional.empty();
	  
	      try {
	        outputValue = bot.getBot().play(bot.getGlobalSessionContext(), this.input);
	      }
	      catch (Exception e) {
	        String message;
	        if (e.getMessage() != null && !e.getMessage().isEmpty()) {
	          message = e.getMessage();
	        }
	        else {
	            
	            message = String.format("Task Bot %s threw an exception : ", new Object[] { e.getClass().getSimpleName() });
	        } 
	        logger.error(message, e);
	        if (!this.continueOnError.booleanValue()) {
	          throw new BotCommandException(message, e);
	        }
	      } 
	      

		    
		    return outputValue.isPresent() ? outputValue.get() : null;     
	    
	    } 
    
    
  }