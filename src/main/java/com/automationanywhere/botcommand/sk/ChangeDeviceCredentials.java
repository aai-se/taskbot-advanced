/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;



import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;

import java.util.concurrent.Semaphore;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import aaicr.utils.CROperations;
import com.automationanywhere.commandsdk.annotations.Execute;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;



/**
 * @author Stefan Karsten
 * 
 * Each child bot should be added as dependency to the parent bot
 *
 */



@BotCommand
@CommandPkg(label="Change Device Credentials", name="changedevicecredentials", description="Change Device Credentails of a User", icon = "alp.svg",  comment = true , text_color = "#666691" , background_color =  "#666691" , 
node_label="Change Device Credentials",
return_type=DataType.STRING, return_label="Result" , return_description = "Change Status" , return_required=true)
public class ChangeDeviceCredentials {
	
	  private static final Logger logger = LogManager.getLogger(ChangeDeviceCredentials.class);
	  
	  @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	  private GlobalSessionContext globalSessionContext;

	  
	  public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	    }
	  
	 @Sessions
	 private Map<String, Object> sessions;
	  

	 
	 
	@Execute
	public StringValue action( @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default" ) @NotEmpty String sessionName,
						@Idx (index = "2", type = TEXT) @Pkg(label = "User Name", default_value_type = STRING) @NotEmpty String user,
						@Idx (index = "3", type = TEXT) @Pkg(label = "Windows Username", default_value_type = STRING) @NotEmpty String windowsuser,
						@Idx (index = "4", type = AttributeType.CREDENTIAL) @Pkg(label = "Windows Password", default_value_type = DataType.STRING) @NotEmpty  SecureString windowspassword
						) throws Exception
	 {
		
		logger.info("Password " + windowspassword.getInsecureString()) ;
	    String result = changeDeviceCredential(user,windowsuser,windowspassword.getInsecureString());
  	    return new StringValue(result);

	}
	
	
	
	
	public  String changeDeviceCredential(String user, String windowsuser, String windowspassword) throws Exception 
	{
	    

	    String crURL = this.globalSessionContext.getCrUrl();
	    String token = this.globalSessionContext.getUserToken();
		logger.info("CR URL " + crURL) ;
		logger.info("Token " + token) ;
	    
	    CROperations crOp = new CROperations();
	    crOp.setCrurl(crURL);
	    crOp.setCrToken(token);
	    String result = crOp.setDeviceCredentials(user, windowsuser, windowspassword);
	    
		return result;
		
	}
	

    
  public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
  }

	  
}


