/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;



import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.bot.service.GlobalSessionContext.ChildBot;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;


import java.util.Optional;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import static com.automationanywhere.commandsdk.model.AttributeType.CHECKBOX;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

import static com.automationanywhere.commandsdk.model.DataType.BOOLEAN;

import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;



/**
 * @author Stefan Karsten
 * 
 * Each child bot should be added as dependency to the parent bot
 *
 */



@BotCommand
@CommandPkg(label="Run Task", name="customruntask", description="Run Task by name", icon = "alp.svg",  comment = true , text_color = "#666691" , background_color =  "#666691" , 
node_label="Custom Run Task",
return_type=DataType.DICTIONARY,  return_required=false)
public class RunTask {
	
	  private static final Logger logger = LogManager.getLogger(RunTask.class);
	  
	  @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	  private GlobalSessionContext globalSessionContext;

	  
	  public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	    }

	  @Sessions
	  private Map<String, Object> sessions;
	  
	@Execute
	public  Value action( @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default" ) @NotEmpty String sessionName,
						@Idx (index = "2", type = TEXT) @Pkg(label = "Bot Name", default_value_type = STRING) @NotEmpty String botname,
						@Idx(index = "3", type = AttributeType.DICTIONARY )  @Pkg(label = "Bot Input"  , default_value_type = DataType.DICTIONARY )  Map<String,Value> inputdict,
						@Idx(index = "4", type = CHECKBOX) @Pkg(label = "Continue on error ", default_value_type = BOOLEAN, default_value="false")  Boolean continueOnError) throws Exception
	 {
		

		 TaskBotSession taskbotsession= ((TaskBotSession)this.sessions.get(sessionName));
		 String botpath = taskbotsession.getBots().get(botname).toString();
		 inputdict = (inputdict == null) ? new HashMap<String, Value>() : inputdict;
		 continueOnError = (continueOnError == null) ? false : continueOnError;
		 Value result = runTask(botpath, inputdict, continueOnError);
  	     return result;

	}
	
	
	
	
	public Value runTask(String botpath,  Map<String,Value> inputdict, Boolean continueOnError) throws Exception 
	{
	    
	    String taskbotUri = TaskBotSession.baseURI+botpath.replace(" ", "%20");

	    logger.info("Task Bot URI : "+taskbotUri);
	    ChildBot bot = ((com.automationanywhere.bot.service.GlobalSessionContext) this.globalSessionContext).getChildBotWithGlobalSessionContext(taskbotUri);

	    Optional<Value> outputValue = Optional.empty();
	    
	  
	      try {
	        outputValue = bot.getBot().play(bot.getGlobalSessionContext(), inputdict);
	      }
	      catch (Exception e) {
	        String message;
	        if (e.getMessage() != null && !e.getMessage().isEmpty()) {
	          message = e.getMessage();
	        }
	        else {
	            
	            message = String.format("Task Bot %s threw an exception.", new Object[] { e.getClass().getSimpleName() });
	        } 
	        logger.error(message, e);
	        if (!continueOnError.booleanValue()) {
	          throw new BotCommandException(message, e);
	        }
	      } 
	      

		    
		    return outputValue.isPresent() ? outputValue.get() : null;     
	    
	    } 
	
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	    
	  
	}


