/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.AttributeType.BOOLEAN;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
import static com.automationanywhere.commandsdk.model.DataType.DICTIONARY;
import static com.automationanywhere.commandsdk.model.DataType.BOOLEAN;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import javafx.application.Platform;




/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Start session", name = "startSession", description = "Start new session",  comment = true , text_color = "#666691" , background_color =  "#666691" , 
icon = "alp.svg", node_label = "start session {{sessionName}}|") 
public class StartSession {
	
    TaskBotSession taskbotdata ;
 
    @Sessions
    private Map<String, Object> sessions;
    
    private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.demo.messages");
    

    
    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
    		 		  @Idx(index = "2", type = AttributeType.DICTIONARY) @Pkg(label = "Bots",   default_value_type = DICTIONARY ) @NotEmpty Map<String, Value> bots,
    		 		  @Idx(index = "3", type = AttributeType.BOOLEAN) @Pkg(label = "Start Launchpad",  default_value_type = DataType.BOOLEAN, default_value = "false")  Boolean launchpad)
    	 throws Exception {
 
        // Check for existing session
        if (sessions.containsKey(sessionName))
            throw new BotCommandException(MESSAGES.getString("Session name in use ")) ;
       
        this.taskbotdata = new TaskBotSession(launchpad, bots);

        this.sessions.put(sessionName, this.taskbotdata);
        
        if (launchpad) {
        	Platform.runLater(new Runnable() {;
        	@Override
        	public void run() {
                try {
                	launchPad();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}         }
        	});
 
        }
    }
 
    
    private void launchPad() throws Exception {

        this.taskbotdata.getLaunchPad().initLaunchpad();
   }
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
    
    
    
    
 
    
    
}