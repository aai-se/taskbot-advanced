/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;

import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;




/**
 * @author Stefan Karsten
 *
 */



@BotCommand
@CommandPkg(label = "Wait", name = "waitSession", description = "Wait to end session", comment = true , text_color = "#666691" , background_color =  "#666691" , 
icon = "alp.svg", node_label = "wait session {{sessionName}}|",
return_type=DataType.BOOLEAN, return_label="Continue", return_required=true) 
public class Wait {
 
    @Sessions
    private Map<String, Object> sessions;
    
    private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.demo.messages");
    
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	  private GlobalSessionContext globalSessionContext;

	  
	  public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	    }
	  
    

    
    
    @Execute
    public Value<Boolean> wait(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", 
    default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName) throws Exception {
 
		TaskBotSession taskbotsession= ((TaskBotSession)this.sessions.get(sessionName));
		 Launchpad launchpad = taskbotsession.getLaunchPad();
	     launchpad.aquire();
	     Boolean continued = (launchpad.getBotName() != "");
	     return (new BooleanValue(continued));
 
    }
 
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
    
    

    
 
    
    
}