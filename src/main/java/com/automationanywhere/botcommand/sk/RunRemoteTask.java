/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;



import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;

import java.util.concurrent.Semaphore;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import aaicr.utils.CROperations;
import com.automationanywhere.commandsdk.annotations.Execute;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;



/**
 * @author Stefan Karsten
 * 
 * Each child bot should be added as dependency to the parent bot
 *
 */



@BotCommand
@CommandPkg(label="Run Remote Task", name="customremoteruntask", description="Run Remote Task by name", icon = "alp.svg",  comment = true , text_color = "#666691" , background_color =  "#666691" , 
node_label="Custom Run Remote Task",
return_type=DataType.DICTIONARY, return_label="Callback Result" , return_description = "Key 'botstatus' contains execution status" , return_required=true)
public class RunRemoteTask {
	
	  private static final Logger logger = LogManager.getLogger(RunRemoteTask.class);
	  
	  @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	  private GlobalSessionContext globalSessionContext;

	  
	  public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	    }
	  
	 @Sessions
	 private Map<String, Object> sessions;
	  
	 private static Semaphore sem;
	 
	 private DictionaryValue callbackResult;
	 
	 
	@Execute
	public DictionaryValue action( @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default" ) @NotEmpty String sessionName,
						@Idx (index = "2", type = TEXT) @Pkg(label = "Bot Name", default_value_type = STRING) @NotEmpty String bot,
						@Idx (index = "3", type = TEXT) @Pkg(label = "Runner Name", default_value_type = STRING) @NotEmpty String runner,
						@Idx (index = "4", type = TEXT) @Pkg(label = "Device Pool", default_value_type = STRING) @NotEmpty String pool,
						@Idx(index = "5", type = AttributeType.DICTIONARY )  @Pkg(label = "Bot Input"  , default_value_type = DataType.DICTIONARY )  Map<String,Value> inputdict,
						@Idx(index = "6", type = AttributeType.NUMBER)  @Pkg(label = "Callback Port"  , default_value_type = DataType.NUMBER)  Number port

						) throws Exception
	 {
		 TaskBotSession taskbotsession= ((TaskBotSession)this.sessions.get(sessionName));
		 Map<String, Value> bots = taskbotsession.getBots();
		String botpath = taskbotsession.getBots().get(bot).toString();	
		Integer callbackport = (port == null) ? 0 : port.intValue();
	    inputdict = (inputdict == null) ? new HashMap<String, Value>() : inputdict;
	    callbackResult = new DictionaryValue();
	    runRemoteTask(botpath, runner, pool,inputdict,callbackport);
  	    return callbackResult;

	}
	
	
	
	
	public  void  runRemoteTask(String botname,String runner,String pool, Map<String,Value> inputdict,Integer port) throws Exception 
	{
	    
  		 Map<String, Value> callbackPayload = new HashMap<String,Value>();
  		Thread thread = null ;
  		WebServer webServer = null;

	    String crURL = this.globalSessionContext.getCrUrl();
	    String token = this.globalSessionContext.getUserToken();
		logger.info("CR URL " + crURL) ;
		logger.info("Token " + token) ;
	    
	    CROperations crOp = new CROperations();
	    crOp.setInputVariables(inputdict);
	    crOp.setCrurl(crURL);
	    crOp.setCrToken(token);
	    crOp.setCruser(runner);


	    botname = botname.replaceAll("/", "\\\\");
	    logger.info("Bot Path "+ botname);
	    crOp.setBotname(botname);
	    crOp.setPoolname(pool);
	    
		crOp.getBotbyName();
		crOp.getPoolbyName();
		crOp.getUserbyName();
		
		

		   
		   
		logger.info("Bot Id " + crOp.getBotId()) ;
		logger.info("Pool Id " + crOp.getPoolId());
		logger.info("User Id " + crOp.getUserId());
		
		
		if (sem == null) {
		  sem = new Semaphore(1);		    
		}
		   
		if ((crOp.getBotId() != null) && (crOp.getUserId() != null) && (crOp.getPoolId() != null))
		{
		      String webPath = "/callback";
		      String host = InetAddress.getLocalHost().getHostName();
			  if (port != 0)
			  {	
			      sem.acquire();
				  crOp.setCallbackurl("http://"+host+":"+port.toString()+webPath);
				  logger.info("Callback URL " + "http://"+host+":"+port.toString()+webPath) ;
				  webServer = new WebServer(webPath, port);
				  thread = new Thread(webServer);
				  thread.start();
				
			  }
			  else {
	           		 callbackPayload.put("botstatus", new StringValue("UNKNOWN"));
	           		 callbackResult.set(callbackPayload); 
			  }
			  crOp.deploybyName();

	    } 
		else
		{
			callbackPayload.put("botstatus", new StringValue("ERROR :  UserID="+crOp.getUserId() + " BotID="+ crOp.getBotId() + " PoolID=" + crOp.getPoolId()));
			callbackResult.set(callbackPayload); 
		};
		
		sem.acquire();
		sem.release();
		
		if (thread != null) {
			webServer.stop();
			thread.interrupt();
		}
		
		
	}
	
	
    private class WebServer implements Runnable {
    	
    	
    	private String path;
    	private Integer port;
    	private HttpServer server;
    	
    	public WebServer(String path ,Integer port) {
    		this.path = path;
    		this.port = port;
    	}
    	
        public void run(){
				try {
					server = HttpServer.create(new InetSocketAddress(port), 0);
    		        server.createContext(path, new CallBackHandler());
    		        server.setExecutor(null); // creates a default executor
    		        server.start();
    		        logger.info("Web Server started with Port "+this.port);
				} catch (IOException e) {
					logger.info("HTTP Server Exception" + e.getMessage()) ;
				}
		}
        
        
        public void stop() {
        	if (server != null) {
        		server.stop(0);
        	}
        	
        }
        
        
        private class CallBackHandler implements HttpHandler {
            @Override
            public void handle(HttpExchange t) throws IOException {
                logger.info("Web Server Request");
            	InputStream in = t.getRequestBody();

                StringBuilder textBuilder = new StringBuilder();
                try (Reader reader = new BufferedReader(new InputStreamReader
                  (in, Charset.forName(StandardCharsets.UTF_8.name())))) {
                    int c = 0;
                    while ((c = reader.read()) != -1) {
                        textBuilder.append((char) c);
                    }
                }
                
                String request = textBuilder.toString();
                
                logger.info("Remote Bot Result "+ request);
            	
                String response = "Received";
                t.sendResponseHeaders(200, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
                
                
                 
            		
           		 JSONObject jsonObj = new JSONObject(request);
           		 String deploymentId = jsonObj.get("deploymentId").toString();
           		 String status = jsonObj.get("status").toString();
           		 logger.info("Callback status " + status );
           		 logger.info("Callback deploymentID " + deploymentId);
           		 
           		 Map<String, Value> callbackPayload = new HashMap<String,Value>();
           		 
           		 callbackPayload.put("botstatus", new StringValue(status));
           		 
           		 if (jsonObj.has("botOutput")) {
           			 
           			 JSONObject botOutput = (JSONObject)jsonObj.get("botOutput");
           			 for (String key : botOutput.keySet()) {
           				 JSONObject output =  (JSONObject) botOutput.get(key);
           				 String type = output.getString("type");
           				 switch (type) {
           				 	case "NUMBER":
           				 		callbackPayload.put(key,new NumberValue(Double.parseDouble(output.getString("number"))));
           				 		break;
           				 	case "STRING":
           				 		callbackPayload.put(key,new StringValue(output.getString("string")));
           				 		break;
           				 	case "BOOLEAN":
           				 		callbackPayload.put(key,new BooleanValue(Boolean.parseBoolean(output.getString("boolean"))));
           				 		break;
           				 	default:
           				 		break;
           				}
           				
           			}
           		 }
                
           		callbackResult.set(callbackPayload);
    		    sem.release();
            }

    }
    
    
 } 
    
  public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
  }

	  
}


